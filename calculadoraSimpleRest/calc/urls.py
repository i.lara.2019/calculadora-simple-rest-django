from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('suma/<int:n1>/<int:n2>', views.suma),
    path('resta/<int:n1>/<int:n2>', views.resta),
    path('mult/<int:n1>/<int:n2>', views.mult),
    path('div/<int:n1>/<int:n2>', views.div),
]
