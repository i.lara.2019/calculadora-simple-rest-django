from django.shortcuts import render
from django.http import HttpResponse

index_html = """
<html>
	<head>
		<title> Calculadora Django </title>
	</head>
	<body>
		<table cellspacing="15" cellpadding="5" border="2">
			<h1> Caluladora en Django </h1>
			<tr>
				<th> Operación Matemática </th>
				<th> Recursos necesarios para su resolución </th>
			</tr>
			<tr>
				<td> Suma </td>
				<td> /suma/num1/num2 </td>
			</tr>
			<tr>
				<td> Resta </td>
				<td> /resta/num1/num2 </td>
			</tr>
			<tr>
				<td> Multiplicación </td>
				<td> /mult/num1/num2 </td>
			</tr>
			<tr>
				<td> División </td>
				<td> /div/num1/num2 </td>
			</tr>
		</table>
	</body>
</html>
"""

op_html = """
<html>
	<head>
		<title> Calculadora Django </title>
	</head>
	<body>
		<h1> El resultado de la {op} de {num1} y {num2} es: {res}</h1>
	</body>
</html>
"""

def index(request):
	return HttpResponse(index_html)
	
def suma(request, n1, n2):
	suma = n1 + n2
	return HttpResponse(op_html.format(op="suma", num1=n1, num2=n2, res=suma))
	
def resta(request, n1, n2):
	resta = n1 - n2
	return HttpResponse(op_html.format(op="resta", num1=n1, num2=n2, res=resta))
	
def mult(request, n1, n2):
	mult = n1 * n2
	return HttpResponse(op_html.format(op="multiplicación", num1=n1, num2=n2, res=mult))

def div(request, n1, n2):
	div = n1 / n2
	return HttpResponse(op_html.format(op="división", num1=n1, num2=n2, res=div))
	
